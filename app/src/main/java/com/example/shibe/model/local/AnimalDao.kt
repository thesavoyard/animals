package com.example.shibe.model.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.shibe.utils.AnimalType

@Dao
interface AnimalDao {

    @Query("SELECT * FROM ANIMAL")
    suspend fun getAll(): List<Animal>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAnimal(vararg animal: Animal)

    @Query("SELECT * FROM Animal WHERE type = :animalType")
    suspend fun getAllTypedAnimals(animalType: AnimalType): List<Animal>
}