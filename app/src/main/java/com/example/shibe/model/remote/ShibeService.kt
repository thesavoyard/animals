package com.example.shibe.model.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface ShibeService {

    companion object {
        private const val BASE_URL = "https://shibe.online"
        private const val ENDPOINT = "/api/shibes"

        fun getInstance(): ShibeService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()
    }

    @GET(ENDPOINT)
    suspend fun getShibes(@Query("count") count: Int = 10): List<String>

}