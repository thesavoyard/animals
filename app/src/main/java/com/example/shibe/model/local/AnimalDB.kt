package com.example.shibe.model.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [Animal::class], version = 1)
abstract class AnimalDB : RoomDatabase(){

    abstract fun AnimalDao(): AnimalDao

    companion object {
        private const val DATABASE_NAME = "Animal.db"

        @Volatile
        private var instance: AnimalDB? = null

        fun getInstance(context: Context): AnimalDB {
            return instance ?: synchronized(this) {
                instance ?: buildDatabes(context).also {
                    instance = it
                }
            }
        }

        private fun buildDatabes(context: Context): AnimalDB {
            return Room
                .databaseBuilder(context, AnimalDB::class.java, DATABASE_NAME)
                .build()
        }
    }

}