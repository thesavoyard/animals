package com.example.shibe.model

import android.content.Context
import android.util.Log
import com.example.shibe.model.local.Animal
import com.example.shibe.model.local.AnimalDB
import com.example.shibe.model.remote.ShibeService
import com.example.shibe.utils.AnimalType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

class ShibeRepo(context: Context) {

    private val shibeService = ShibeService.getInstance()
    private val shibeDao = AnimalDB.getInstance(context).AnimalDao()

    suspend fun getShibes(): List<Animal> = withContext(Dispatchers.IO) {
        val cachedShibes = shibeDao
            .getAllTypedAnimals(AnimalType.SHIBE)
        return@withContext cachedShibes.ifEmpty {
            //Go to the network
            val remoteDaggos = shibeService.getShibes(10)

            // Map our list of strings to a shibe entity
            val entities: List<Animal> = remoteDaggos.map {
                Log.e("TAG", "getShibes: mapping a dog -> $it", )
                Animal(type = AnimalType.SHIBE, image = it)
            }

            // Save Shibes
                shibeDao.insertAnimal(*entities.toTypedArray())

            // return a list of all shibes
            return@ifEmpty entities
        }
    }


}