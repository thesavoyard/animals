package com.example.shibe.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shibe.model.ShibeRepo
import com.example.shibe.model.local.Animal
import kotlinx.coroutines.launch

class ShibeViewModel(private val repo: ShibeRepo) : ViewModel() {

    private val _shibes: MutableLiveData<ShibeState> = MutableLiveData(ShibeState())
    val shibes: LiveData<ShibeState> get() = _shibes


    fun getShibes() {
        viewModelScope.launch {
            _shibes.value = ShibeState(isLoading = true)
            val result = repo.getShibes()
            _shibes.value = ShibeState(shibes = result, isLoading = false)
        }
    }

    data class ShibeState(
        val isLoading : Boolean = false,
        val shibes: List<Animal> = emptyList()
    )

}

