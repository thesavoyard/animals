package com.example.shibe.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.shibe.databinding.DogItemBinding
import com.example.shibe.model.local.Animal

class ShibeAdpater : RecyclerView.Adapter<ShibeAdpater.ShibeViewHolder>() {

    private var list: MutableList<Animal> = mutableListOf()

    fun updateList(newList: List<Animal>) {
        val oldSize = list.size
        list.clear()
        notifyItemRangeChanged(0, oldSize)
        list.addAll(newList)
        notifyItemRangeChanged(0, newList.size)
    }

    class ShibeViewHolder(private val binding: DogItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun displayImage(animal: Animal) {
            binding.ivDogImage.load(animal.image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShibeViewHolder {
        return ShibeViewHolder(
            DogItemBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        holder.displayImage(list[position])
    }

    override fun getItemCount(): Int = list.size


}