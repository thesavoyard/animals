package com.example.shibe.view

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import com.example.shibe.databinding.ActivityMainBinding
import com.example.shibe.model.ShibeRepo
import com.example.shibe.viewmodel.ShibeVMFactory
import com.example.shibe.viewmodel.ShibeViewModel

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    private lateinit var vmFactory: ShibeVMFactory
    private val viewModel: ShibeViewModel by viewModels() { vmFactory }

    private val theAdapter: ShibeAdpater by lazy {
        ShibeAdpater()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        vmFactory = ShibeVMFactory(ShibeRepo(this))
        initViews()
        initObservers()
        setContentView(binding.root)
        viewModel.getShibes()
    }

    private fun initViews() {
        with(binding.rvDoggy) {
            layoutManager = GridLayoutManager(this@MainActivity, 2)
            adapter = theAdapter
        }

    }

    fun initObservers() {
        viewModel.shibes.observe(this) { state ->
            binding.progress.isVisible = state.isLoading
            theAdapter.updateList(state.shibes)
        }
    }

}