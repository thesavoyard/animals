package com.example.shibe.utils

enum class AnimalType {
    SHIBE, CATE, BIRD
}